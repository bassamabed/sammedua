Sam Media Assesment
===========

### Development Environment Setup
- Install Nodejs [v16.2.0]
- Install GIT
- Install React Developer Tools for Google Chrome.
- Go to Project root directory and run `yarn install`. This will install Project dependencies.
- Run `yarn start`. This will start development node server. 
- Go to http://localhost:3000/ from Browser.

[NodeJS v16.2.0]: http://nodejs.org/