import React, { Component } from 'react';
import logo from './assest/logo.png';
import badge from './assest/badge.png';
import phone from './assest/phone.png';
import virusScan from './assest/free-scan.png';
import iconDownload from './assest/download.png';
import clock from './assest/clock-circular-outline.png';
import {withRouter} from 'react-router-dom'

import './screen1.scss';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="appLogo" alt="logo" />
          <img src={badge} className="appBadge" alt="logo" />
        </header>
        <div className="textBox">
          <h1 className="textBody">is your device protected?</h1>
          <h2 className="textBody2">find out with our free scan now</h2>
        </div>
        <div className="virusContainer">
          <img className="virusImg" src={virusScan} alt="anti virus" />
          <p className="virusText">free</p>
          <span className="virusText scan">virus scan</span>
          <img className="phoneImg" src={phone} alt="logo" />
        </div>
        <div className="scanBox">
        <div className="scanPeriod">
          <p className="freeText">free scan ends in</p>
          <p className="freeTime">00:15</p>
        </div>
          <button onClick={()=> this.props.history.push("/screen2")} className="scanBtn">scan my device now</button>
        <div className="downNum">
        <div className="downImg">
          <img className="iconDownload" src={iconDownload} />
        </div>
        <div className="downTex">
          <p className="downText">78,169 Downloads</p>
          <small className="downSmall">in the last 7 days</small>
        </div>
        </div>
        <div className="recenNum">
        <div className="recenImg">
          <img className="iconDownload" src={clock} />
        </div>
        <div className="downTex">
          <p className="downText">Most Recent Download</p>
          <small className="recenSmall">Less than a minute ago</small>
        </div>
        </div>
        </div>
      </div>
    );
  }
}

 
export default withRouter(App);
