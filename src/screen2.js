import React, { Component } from 'react';
import logo from './assest/logo.png';
import badge from './assest/badge.png';
import phone from './assest/phone.png';
import flag from './assest/flag.png';
import gift from './assest/gift.png';
import iconDownload from './assest/download.png';
import clock from './assest/clock-circular-outline.png';
import attach from './assest/attach.png';
import happy from './assest/happy.png';

import './screen2.scss';

class App extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      showRegister:false,
      showSendSms: false
    };
  }

  showRegisterModal(payload){
    this.setState({showRegister:payload})
  }
  showSendSMS(payload){
    this.setState({showRegister:false});
    this.setState({showSendSms:payload});
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="appLogo" alt="logo" />
          <img src={badge} className="appBadge" alt="logo" />
        </header>
        <div className="textBox">
          <h1 className="textBody">Scannig your device now!</h1>
        </div>
        <div className="virusContainer">
          <hr className="virusLine" />
          <p className="virusProg">90%</p>
          <img className="phoneImg" src={phone} alt="logo" />
        </div>
        <div className="scanBox">
        <div className="scanPeriod">
          <p className="freeText">possible infections have been detected</p>
        </div>
        <div className="rowText">
        <div className="infName">
          <p>Infections found</p>
          <p>Infected objects removed</p>
          <p>Spyware found</p>
          <p>Spyware removed</p>
          <p>Not removed</p>
        </div>
        <div className="infNum">
          <p>0</p>
          <p>0</p>
          <p>0</p>
          <p>0</p>
          <p>0</p>
        </div>
        </div>
          <button className="scanBtn" onClick={()=>this.showRegisterModal(true)}>scan my device now</button>
        </div>
          {
            this.state.showRegister &&
            <div className="modal-container">
              <div className="modal">
                <div className="gift">
                  <img src={gift} />
                </div>
                <span>To start cleaning, register NOW & get FREE memory booster</span>
                
                  <div className="input-contianer">
                    <label>Enter phone to start cleaning</label>
                    <div className="input-wrapper">
                      <div className="logo-code"><img src={flag} /><div className="code">(+60)</div></div>
                      <input placeholder="mobile number"/>
                    </div>
                    <button onClick={()=>this.showSendSMS(true)} className="register">scan my device now</button>
                  </div>
                  <div className="downNum">
                  <div className="downImg">
                    <img className="iconDownload" src={iconDownload} />
                  </div>
                  <div className="downTex">
                    <p className="downText">78,169 Downloads</p>
                    <small className="downSmall">in the last 7 days</small>
                  </div>
                  </div>
                  <div className="recenNum">
                  <div className="recenImg">
                    <img className="iconDownload" src={clock} />
                  </div>
                  <div className="downTex">
                    <p className="downText">Most Recent Download</p>
                    <small className="recenSmall">Less than a minute ago</small>
                  </div>
                  </div>
              </div>
            </div>
          }

          {
            this.state.showSendSms &&
            <div className="modal-container">
              <div className="modal">
                <div className="send-sms">
                  <p>SEND SMS TO CLAIM FREE VIRUS SCAN</p>
                </div>
                <span>Send <i>1234</i> to <i>76655</i></span>
                <img className="iconAttach" src={attach} />
                <input type="text" />
                <img className="iconHappy" src={happy} />
                <input type="submit" />
                 <button onClick={()=>this.showSendSMS(false)} className="register">scan my device now</button>
                 <div className="download-num">
                  <p>Over 200K Downloads</p>
                </div>
              </div>
            </div>
          }
      </div>
    );
  }
}

export default App;
