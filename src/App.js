import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import scrrenOne from "./screen1";
import scrrenTwo from "./screen2";

const BasicExample = () => (
  <Router>
    <div>
      <ul className="mainNav">
        <li>
          <Link className="routLinks" to="/">screen One</Link>
          <Link className="routLinks" to="/screen2">screen Two</Link>
        </li>
      </ul>

      <Route exact path="/" component={scrrenOne} />
      <Route path="/screen2" component={scrrenTwo} />
    </div>
  </Router>
);


export default BasicExample;